# RefactorThis

## Setup
Clone the repo to your desired directory.

Enter the new directory

``` bash
$ cd refacorthis
```

Install the Node.js packages

``` bash
$ npm install
```

## Scripts
To start the server:
``` bash
$ npm run start
```
To start the dev server that will reload on code change:
``` bash
$ npm run dev
```
To run the code through linting:
``` bash
$ npm run lint
```
To run the test files:
``` bash
$ npm run test
```

## Authentication
Application currently uses a basic auth system with a Bearer Token.
The token will be logged in the console on startup and can be used in postman to access the API.

This could be replaced with a login system later on using passport.
https://www.npmjs.com/package/passport

## CI Stages
CI runs on each commit.

### Install Dependencies Stage
This will run ```npm install``` and save the node_modules folder in cache so the other stages don't need to run the install command every time.

### Test Stage
#### Lint
This will run the lint command against the code and fail if there are any errors.
#### Security
This will do an audit on the packages used and throw an error if there is any security issues.
#### Test
This will run the test files and fail if any tests fail.

## API Endpoints

1. `GET /products` - gets all products.
2. `GET /products?name={name}` - finds all products matching the specified name.
3. `GET /products/{id}` - gets the project that matches the specified ID - ID is a GUID.
4. `POST /products` - creates a new product.
5. `PUT /products/{id}` - updates a product.
6. `DELETE /products/{id}` - deletes a product and its options.

7. `GET /products/{id}/options` - finds all options for a specified product.
8. `GET /products/{id}/options/{optionId}` - finds the specified product option for the specified product.
9. `POST /products/{id}/options` - adds a new product option to the specified product.
10. `PUT /products/{id}/options/{optionId}` - updates the specified product option.
11. `DELETE /products/{id}/options/{optionId}` - deletes the specified product option.

All routes are specified in the `/routes/api` folder, and conform to:

**Product:**
```
{
  "Id": "01234567-89ab-cdef-0123-456789abcdef",
  "Name": "Product name",
  "Description": "Product description",
  "Price": 123.45,
  "DeliveryPrice": 12.34
}
```

**Products:**
```
{
  "Items": [
    {
      // product
    },
    {
      // product
    }
  ]
}
```

**Product Option:**
```
{
  "Id": "01234567-89ab-cdef-0123-456789abcdef",
  "Name": "Product name",
  "Description": "Product description"
}
```

**Product Options:**
```
{
  "Items": [
    {
      // product option
    },
    {
      // product option
    }
  ]
}
```