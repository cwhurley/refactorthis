const app = require('../app');
const sqlite3 = require('sqlite3').verbose();
const request = require('supertest');
var db = require("../util/database.js")

beforeAll(() => {
  process.env.NODE_ENV = 'test';
})

const seedDb = db => {
  // Create Products table
  db.run('CREATE TABLE IF NOT EXISTS Products (Id TEXT PRIMARY KEY, Name TEXT, Description TEXT, Price FLOAT, DeliveryPrice FLOAT)');
  db.run('DELETE FROM Products');
  // Insert data into table
  const stmt = db.prepare('INSERT INTO Products (id, name, description, price, deliveryprice) VALUES (?, ?, ?, ?, ?)');
  const stmt2 = db.prepare('INSERT INTO Products (id, name, description, price, deliveryprice) VALUES (?, ?, ?, ?, ?)');
  stmt.run('8F2E9176-35EE-4F0A-AE55-83023D2DB1A3', 'Samsung Galaxy S7', 'Newest mobile product from Samsung.', 1024.99, 16.99);
  stmt2.run('DE1287C0-4B15-4A7B-9D8A-DD21B3CAFEC3', 'Apple iPhone 6S', 'Newest mobile product from Apple.', 1299.99, 15.99)
  // Create ProductsOptions table
  db.run('CREATE TABLE IF NOT EXISTS ProductOptions (Id TEXT PRIMARY KEY, ProductId TEXT, Name TEXT, Description TEXT)');
  db.run('DELETE FROM ProductOptions');
  // Insert data into table
  const stmt3 = db.prepare('INSERT INTO ProductOptions (id, productid, name, description) VALUES (?, ?, ?, ?)');
  stmt3.run('0643CCF0-AB00-4862-B3C5-40E2731ABCC9', '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3', 'White', 'White Samsung Galaxy S7');
  stmt.finalize();
}

describe('/api/product end points', function () {

  it('Return two items', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products/');
      const response = {
        Items: [
          {
            Id: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
            Name: 'Samsung Galaxy S7',
            Description: 'Newest mobile product from Samsung.',
            Price: 1024.99,
            DeliveryPrice: 16.99
          },
          {
            Id: 'DE1287C0-4B15-4A7B-9D8A-DD21B3CAFEC3',
            Name: 'Apple iPhone 6S',
            Description: 'Newest mobile product from Apple.',
            Price: 1299.99,
            DeliveryPrice: 15.99
          }
        ]
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Return samsung entry when searching', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products?name=samsung');
      const response = {
        Items: [
          {
            Id: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
            Name: 'Samsung Galaxy S7',
            Description: 'Newest mobile product from Samsung.',
            Price: 1024.99,
            DeliveryPrice: 16.99
          }
        ]
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Return samsung entry', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3');
      const response = {
        Id: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
        Name: 'Samsung Galaxy S7',
        Description: 'Newest mobile product from Samsung.',
        Price: 1024.99,
        DeliveryPrice: 16.99
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('No rows found', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products/8F2E9176-35EE-4F0A');
      const response = {
        message: 'No rows found.'
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Create product', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).post('/api/products/').send({
        name: 'OnePlus 8T',
        description: 'Newest mobile product from OnePlus',
        price: 999.99,
        deliveryprice: 19.99
      });
      const response = {
        message: 'success',
        data: {
          id: '602d159d-92cc-42bf-a684-7aa41db4ee5d',
          name: 'OnePlus 8T',
          description: 'Newest mobile product from OnePlus',
          price: 999.99,
          deliveryprice: 19.99
        }
      }
      expect(res.status).toBe(200);
      expect(res.body.message).toEqual(response.message);
      done()
    })
  });

  it('Create product', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).post('/api/products/').send({
        name: 'OnePlus 8T',
        price: 999.99,
        deliveryprice: 19.99
      });
      const response = { error: 'No description specified' }

      console.log(res.body)
      expect(res.status).toBe(400);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Update product', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).put('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3').send({
        name: 'Samsung Galaxy S7',
        description: 'Newest mobile product from Samsung.',
        price: 999.99,
        deliveryprice: 16.99
      });
      const response = {
        message: 'success',
        data: {
          name: 'Samsung Galaxy S7',
          description: 'Newest mobile product from Samsung.',
          price: 999.99,
          deliveryprice: 16.99
        },
        changes: 1
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Update product fail due to wrong id.', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).put('/api/products/8F2E9176-35EE-4F').send({
        name: 'Samsung Galaxy S7',
        description: 'Newest mobile product from Samsung.',
        price: 999.99,
        deliveryprice: 16.99
      });
      const response = { message: "Unable to make change."}
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Delete product', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).delete('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3')
      const response = { message: 'deleted', changes: 1 }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Return samsung options entries', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3/options');
      const response = {
        Items: [
          {
            Id: '0643CCF0-AB00-4862-B3C5-40E2731ABCC9',
            ProductId: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
            Name: 'White',
            Description: 'White Samsung Galaxy S7'
          }
        ]
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Return option entry', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3/options/0643CCF0-AB00-4862-B3C5-40E2731ABCC9');
      const response = {
            Id: '0643CCF0-AB00-4862-B3C5-40E2731ABCC9',
            ProductId: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
            Name: 'White',
            Description: 'White Samsung Galaxy S7'
          }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Create product option', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).post('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3/options').send({
        name: 'Black',
        description: 'Black Samsung Galaxy S7'
      });
      const response = {
        message: 'success',
        data: {
          id: '3a326953-6d96-4d53-9c8f-49af3384e367',
          productid: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
          name: 'Black',
          description: 'Black Samsung Galaxy S7'
        }
      }
      expect(res.status).toBe(200);
      expect(res.body.message).toEqual(response.message);
      done()
    })
  });

  it('Update option', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).put('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3/options/0643CCF0-AB00-4862-B3C5-40E2731ABCC9').send({
        name: 'Green',
        description: 'Green Samsung Galaxy S7'
      });
      const response = {
        message: 'success',
        data: {
          productid: '8F2E9176-35EE-4F0A-AE55-83023D2DB1A3',
          name: 'Green',
          description: 'Green Samsung Galaxy S7'
        },
        changes: 1
      }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Delete option', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).delete('/api/products/8F2E9176-35EE-4F0A-AE55-83023D2DB1A3/options/0643CCF0-AB00-4862-B3C5-40E2731ABCC9').send({
        name: 'Green',
        description: 'Green Samsung Galaxy S7'
      });
      const response = { message: 'deleted', changes: 1 }
      expect(res.status).toBe(200);
      expect(res.body).toEqual(response);
      done()
    })
  });

  it('Endpoint not found', async function (done) {
    await db.serialize(async () => {
      seedDb(db);
      const res = await request(app).get('/api/option')
      const response = { message: 'API endpoint does not exist.' }
      expect(res.status).toBe(404);
      expect(res.body).toEqual(response);
      done()
    })
  });
})
