const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
let db = require("../../util/database.js")

const noRowsFound = {
  message: 'No rows found.'
}
const unableToChange = {
  message: 'Unable to make change.'
}

/* router structure:
- Create sql statement with necessary error checking
- Handle paramaters to be used
- Do database call with error handling
*/

// Return all products or search using name
router.get('/', (req, res) => {
  const sql = (req.query.name === undefined) ? 'select * from Products' : 'select * from Products where Name like ?'
  const params = (req.query.name === undefined) ? [] : [`%${req.query.name}%`]
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.json({
      "Items": rows
    })
  });
});

// Get specific product
router.get('/:id', (req, res) => {
  const sql = "select * from Products where id = ?"
  const params = [req.params.id]
  db.get(sql, params, (err, rows) => {
    if (err) {
      console.log(err)
      res.status(400).json({ "error": err.message });
      return;
    }
    const response = (rows === undefined) ? noRowsFound : rows
    res.json(response)
  });
});

// Create a new product
router.post('/', (req, res) => {
  // Check if any of the required fileds are empty and return an error to user.
  let errors = []
  if (!req.body.name) {
    errors.push("No name specified");
  }
  if (!req.body.description) {
    errors.push("No description specified");
  }
  if (!req.body.price) {
    errors.push("No price specified");
  }
  if (!req.body.deliveryprice) {
    errors.push("No delivery price specified");
  }
  if (errors.length) {
    res.status(400).json({ "error": errors.join(",") });
    return;
  }
  const data = {
    id: uuidv4(),
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    deliveryprice: req.body.deliveryprice,
  }
  const sql = "insert into Products (Id, Name, Description, Price, DeliveryPrice) values (?, ?, ?, ?, ?)"
  const params = [data.id, data.name, data.description, data.price, data.deliveryprice]
  db.run(sql, params, (err) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.json({
      "message": "success",
      "data": data,
    })
  });
});

// Update a product
router.put('/:id', (req, res) => {
  const data = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    deliveryprice: req.body.deliveryprice,
  }
  db.run(
    `update Products set 
      name = COALESCE(?,name), 
      description = COALESCE(?,description), 
      price = COALESCE(?,price), 
      deliveryprice = COALESCE(?,deliveryprice) 
      WHERE id = ?`,
    [data.name, data.description, data.price, data.deliveryprice, req.params.id],
    function (err) {
      if (err) {
        res.status(400).json({ "error": res.message })
        return;
      }
      const response = (this.changes === 0) ? unableToChange : {
        message: "success",
        data: data,
        changes: this.changes
      }
      res.json(response)
    });
})

// Delete a product
router.delete('/:id', (req, res) => {
  db.run(
    'DELETE FROM Products WHERE id = ?',
    req.params.id,
    function (err) {
      if (err) {
        res.status(400).json({ "error": res.message })
        return;
      }
      const response = (this.changes === 0) ? unableToChange : { "message": "deleted", changes: this.changes }
      res.json(response)
    });
})

// Get product details for a specific product
router.get('/:id/options', (req, res) => {
  const sql = "select * from ProductOptions where ProductId = ?"
  const params = [req.params.id]
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    const response = (rows.length === 0) ? noRowsFound : {"Items": rows}
    res.json(response)
  });
});

// Get the details for one option for a specific product
router.get('/:id/options/:optionId', (req, res) => {
  const sql = "select * from ProductOptions where id = ?"
  const params = [req.params.optionId]
  db.get(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    const response = (rows === undefined) ? noRowsFound : rows
    res.json(response)
  });
});

// Create new detail option for a specific product
router.post('/:id/options', (req, res) => {
  // Check if any of the required fileds are empty and return an error to user.
  let errors = []
  if (!req.body.name) {
    errors.push("No name specified");
  }
  if (!req.body.description) {
    errors.push("No description specified");
  }
  if (errors.length) {
    res.status(400).json({ "error": errors.join(",") });
    return;
  }
  const data = {
    id: uuidv4(),
    productid: req.params.id,
    name: req.body.name,
    description: req.body.description
  }
  const sql = "insert into ProductOptions (Id, ProductId, Name, Description) values (?, ?, ?, ?)"
  const params = [data.id, data.productid, data.name, data.description]
  db.run(sql, params, (err) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.json({
      "message": "success",
      "data": data,
    })
  });
});

// Update a detail option for a specific product
router.put('/:id/options/:optionId', (req, res) => {
  const data = {
    productid: req.params.id,
    name: req.body.name,
    description: req.body.description,
  }
  db.run(
    `update ProductOptions set 
      productid = COALESCE(?,productid), 
      name = COALESCE(?,name), 
      description = COALESCE(?,description) 
      WHERE id = ?`,
    [data.productid, data.name, data.description, req.params.optionId],
    function (err) {
      if (err) {
        res.status(400).json({ "error": res.message })
        return;
      }
      const response = (this.changes === 0) ? unableToChange : {
        message: "success",
        data: data,
        changes: this.changes
      }
      res.json(response)
    });
})

// Delete a detail option
router.delete('/:id/options/:optionId', (req, res) => {
  db.run(
    'DELETE FROM ProductOptions WHERE id = ?',
    req.params.optionId,
    function (err) {
      if (err) {
        res.status(400).json({ "error": res.message })
        return;
      }
      const response = (this.changes === 0) ? unableToChange : { "message": "deleted", changes: this.changes }
      res.json(response)
    });
})

module.exports = router;
