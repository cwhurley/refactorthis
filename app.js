const express = require('express');
const products = require('./routes/api/products');
const nJwt = require('njwt');
const secureRandom = require('secure-random');
// Create a highly random byte array of 256 bytes
const signingKey = secureRandom(256, { type: 'Buffer' });
const app = express();
const bodyParser = require("body-parser");

let claims = {
  iss: "http://localhost:3000/"
}
// Create jwt token
let jwt = nJwt.create(claims, signingKey);
// Use code below to add expiration time to token.
// jwt.setExpiration(new Date().getTime() + (60*60*1000));
const accessTokenSecret = jwt.compact();
// Log token to use for testing
console.log('Access Token:')
console.log(accessTokenSecret);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// This function checks if the user has sent a valid token with the request.
function authenticate(req, res, next) {
  // If unit tests are running, skip auth check.
  if (process.env.NODE_ENV === 'test') {
    next()
  } else {
    const authHeader = req.headers['authorization']
    // If no auth token was sent, return 401.
    if (authHeader == null) {
      return res.sendStatus(401)
    }
    const token = authHeader.split(' ')[1]
    // Check if valid taken was sent, return 401 if invalid. If valid, continue to api request.
    nJwt.verify(token, signingKey, (err) => {
      return !err ? next() : res.sendStatus(401)
    });
  }
}

const port = process.env.NODE_ENV === 'test' ? 3001 : 3000;

// API Routes
app.use('/api/products', authenticate, products);

// Invalid endpoint check
app.use((req, res) => {
  const response = {
    message: 'API endpoint does not exist.'
  }
  res.status(404).json(response)
});

app.listen(port, () => {
  console.log("Server running on port " + port);
});

module.exports = app;
