let sqlite3 = require('sqlite3').verbose()
// Load database file
const DBSOURCE = process.env.NODE_ENV === 'test' ? ':memory:' : 'data/products.db'

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message)
    throw err
  } else {
    console.log('SQLITE database starting up. ' + DBSOURCE)
  }
});

module.exports = db